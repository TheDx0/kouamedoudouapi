<?php

namespace App\Models;

use App\Models\Compte;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Abonne extends Model
{
    use HasFactory;
    protected $fillable = [
        'nom', 'prenom', 'email', 'contact', 'active','abonne_id',
        // Ajoutez d'autres colonnes si nécessaire
    ];

    public function comptes()
    {
        return $this->hasMany(Compte::class);
    }
}
