<?php

namespace App\Models;

use App\Models\Abonne;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Compte extends Model
{
    use HasFactory;
    protected $fillable = ['abonne_id', 'libelle', 'description', 'agence', 'banque', 'numero', 'rib', 'montant', 'domiciliation'];

    public function abonne()
    {
        return $this->belongsTo(Abonne::class);
    }
}
