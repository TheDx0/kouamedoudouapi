<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use App\Models\Compte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CompteController extends Controller
{
    public function index(Request $request, $iban=null)
    {
        // Retourner la liste des comptes avec iban en paramètre
        $iban = $request->get('iban');
        if($iban){

            $compte=null;
            foreach(Compte::all() as $row){

                $totaliban = $row['banque'].$row['agence'].$row['numero'].$row['rib'];
                if($iban == $totaliban){
                    $compte = $totaliban;
                    break;
                }
            }

            if($compte==null){

                return response()->json([
                    "error"=>true,
                    "message"=>"aucun compte trouve avec cet iban:".$iban
                ]);
            }

            $abonnes = Abonne::with('comptes')->get();
            return response()->json([
                "error"=>false,
                "message"=>"compte trouve avec succes " . $compte,
                "data"=>$abonnes
            ]);

        }

        // Retourner la liste des comptes
        return response()->json(Compte::all());

    }

    public function show($id)
    {
        // Retourner les détails d'un compte
        $compte = Compte::find($id);

    if (!$compte) {
        return response()->json(['message' => 'Compte non trouvé'], 404);
    }

    return response()->json(['data' => $compte], 200);
    }

    public function store(Request $request)
    {
        // Ajouter un nouveau compte
        $request->validate([
            'abonne_id' => 'required|exists:abonnes,id',
            'libelle' => 'required',
            'description' => 'nullable',
            'agence' => 'required|numeric',
            'banque' => 'required|string',
            'numero' => 'required|numeric',
            'rib' => 'required|numeric',
            'montant' => 'required|numeric',
            'domiciliation' => 'required',
        ]);
    
        $compte = Compte::create($request->all());
    
        return response()->json(['data' => $compte], 201);
    }

    public function update(Request $request, $id)
    {
        // Mettre à jour les informations d'un compte
        $compte = Compte::find($id);

        if (!$compte) {
            return response()->json(['message' => 'Compte non trouvé'], 404);
        }

        $request->validate([
            'abonne_id' => 'required|exists:abonnes,id',
            'libelle' => 'required',
            'description' => 'nullable',
            'agence' => 'required|numeric',
            'banque' => 'required|string',
            'numero' => 'required|numeric',
            'rib' => 'required|numeric',
            'montant' => 'required|numeric',
            'domiciliation' => 'required',
        ]);

        $compte->update($request->all());
        return response()->json(['data' => $compte], 200);
    }

    public function destroy($id)
    {
        // Supprimer un compte
        $compte = Compte::find($id);

        if (!$compte) {
            return response()->json(['message' => 'Compte non trouvé'], 404);
        }
    
        $compte->delete();
        return response()->json(['message' => 'Compte supprimé avec succès'], 200);
    }

    public function stats(){

        // Statistiques générales
        $effectifabonne = Abonne::count();
        $totalmontant = Compte::sum('montant');
        $effectifcompte = Compte::count();
        $minmontant = Compte::min('montant');
        $maxmontant = Compte::max('montant');
        $averagemontant = Compte::avg('montant');

        return response()->json([
            ''=>"STATISTIQUE GENERAL",
            
                'Effectif abonnes'=>$effectifabonne,
                'Effectif comptes'=>$effectifcompte,
                'total montant'=>$totalmontant,
                'montant minimal'=>$minmontant,
                'montant maximal'=>$maxmontant,
                'la moyenne'=>$averagemontant,
        ]);
    }

    public function statsOnAbonnes($id){

        // Statistiques d'un abonné
        $abonne = Abonne::find($id);
        if ($abonne == null) {
            return response()->json([
                "error" => true,
                "message" => "Aucun abonné trouvé avec cet id : " . $id
            ]);
        }

        $stats = Compte::where('abonne_id', $id)->select(
            DB::raw('COUNT(*) as EffectifCompteAbonne'),
            DB::raw('SUM(montant) as MontantTotal'),
            DB::raw('MIN(montant) as MontantMin'),
            DB::raw('MAX(montant) as MontantMax'),
            DB::raw('AVG(montant) as MoyenneMontant')
        )->first();

        return response()->json([
            "data" => $stats
        ]);

    }

    public function liaison(Request $request){

        // Lier un abonné et un compte
        $validate = Validator::make($request->all(), [
            "abonneId" => "required",
            "compteId" => "required",
        ]);

        if ($validate->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validate->errors()->first()
            ]);
        }

        $abonneId=$request->post('abonneId');
        $compteId=$request->post('compteId');
        
        $compte = Compte::find($compteId);
        if ($compte == null) {
            return response()->json([
                "error" => true,
                "message" => "Aucun compte trouvé avec cet id : " . $compteId
            ]);
        }
       
        $compte->update([
            "abonne_id" =>$abonneId,
        ]);

        return response()->json([
            'error' => false,
            'message' => "Abonne modifier avec succes",
        ]);
    }
}
