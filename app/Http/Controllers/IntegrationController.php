<?php

namespace App\Http\Controllers;

use App\Models\Integration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class IntegrationController extends Controller
{
    public function randomPerson() {

        // Appel à ADMINISTRATIVE-DIVISION-DB
        $regionApiResponse = Http::get("https://rawcdn.githack.com/kamikazechaser/administrative-divisions-db/master/api/CI.json");

        // Vérifier si l'appel a réussi
        if ($regionApiResponse->successful()) {
            $regionData = json_decode($regionApiResponse->body());

            // Créer la réponse JSON pour l'API /personnes/random
            $response = [
                'langue' =>"Français",
                'genre' =>"Masculin",
                'religion' =>"Muslim",
                'pays' =>"Côte d'Ivoire",
                'indicatif' =>"CI",
                'internet' => true,
                'regions' => $regionData,
            ];

            return response()->json($response);
        } else {
            return response()->json(['error' => 'Une ou plusieurs requêtes ont échoué.'], 500);
        }
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('auth-token')->plainTextToken;

            return response()->json(['token' => $token]);
        }

        return response()->json(['message' => 'Unauthorized'], 401);
    }

}
