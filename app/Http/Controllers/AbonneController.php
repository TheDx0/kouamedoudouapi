<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AbonneController extends Controller
{
    public function index()
    {
        // Retourner la liste des abonnés
        $abonnes = Abonne::all();
        return response()->json($abonnes);
    }

    public function show($id)
    {
        // Retourner les détails d'un abonné
        $abonne = Abonne::find($id);

        if (!$abonne) {
            return response()->json(['message' => 'Abonné non trouvé'], 404);
        }

        return response()->json(['data' => $abonne], 200);
    }

    public function store(Request $request)
    {
        // Ajouter un nouvel abonné
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email|unique:abonnes',
            'contact' => 'required|numeric',
            'active' => 'required|boolean',
        ]);
    
        $abonne = Abonne::create($request->all());
    
        return response()->json(['data' => $abonne], 201);
    }

    public function update(Request $request, $id)
    {
        // Mettre à jour les informations d'un abonné
        $abonne = Abonne::find($id);

        if (!$abonne) {
            return response()->json(['message' => 'Abonné non trouvé'], 404);
        }

        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email|unique:abonnes,email,' . $abonne->id,
            'contact' => 'required|numeric',
            'active' => 'required|boolean',
        ]);

        $abonne->update($request->all());

        return response()->json(['data' => $abonne], 200);
    }

    public function destroy($id)
    {
        // Supprimer un abonné
        $abonne = Abonne::find($id);

        if (!$abonne) {
            return response()->json(['message' => 'Abonné non trouvé'], 404);
        }
    
        $abonne->delete();
    
        return response()->json(['message' => 'Abonné supprimé avec succès'], 200);
    }

    public function getAbonneComptes()
    {
        // Récupérez tous les abonnés avec leurs comptes
        $abonnes = Abonne::with('comptes')->get();

        return response()->json($abonnes, 200);
    }

    public function getOnAbonneComptes($id)
    {
        // Récupérez l'abonné avec son ID et ses comptes associés
        $abonne = Abonne::with('comptes')->find($id);

        if (!$abonne) {
            return response()->json(['message' => 'Abonné non trouvé.'], 404);
        }

        return response()->json($abonne, 200);
    }
   
}
