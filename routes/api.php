<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AbonneController;
use App\Http\Controllers\CompteController;
use App\Http\Controllers\IntegrationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/v1/login', [IntegrationController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    // Abonnés
    Route::prefix('v1')->group(function () {
        
        Route::prefix('abonnes')->group(function () {
                
            Route::get('comptes', [AbonneController::class, 'getAbonneComptes']);
            Route::get('{id}/comptes', [AbonneController::class, 'getOnAbonneComptes']);

            Route::get('/',[AbonneController::class,'index']); //Liste abonnées
            Route::get('{id}',[AbonneController::class,'show']); //Détail abonnée
            Route::post('/',[AbonneController::class,'store']); //Ajout abonnée;
            Route::put('{id}', [AbonneController::class, 'update']); //update abonnes
            Route::delete('{id}', [AbonneController::class, 'destroy']); //delete abonnes  
        });

        // Comptes
        Route::prefix('comptes')->group(function () {
            Route::get('/',[CompteController::class,'index']); //Liste comptes
            Route::get('{id}',[CompteController::class,'show']); //Détail comptes
            Route::post('/',[CompteController::class,'store']);//Ajout comptes;
            Route::put('{id}', [CompteController::class, 'update']);//update comptes
            Route::delete('{id}', [CompteController::class, 'destroy']);//supprimer comptes
        });

        // Statistiques
        Route::prefix('stats')->group(function () {
            Route::get('/', [CompteController::class, 'stats']);
            Route::get('abonnes/{id}', [CompteController::class, 'statsOnAbonnes']);
        });

        // Liaison
        Route::post('/liaisons', [CompteController::class, 'liaison']);
        
        Route::get('/personnes/random',[IntegrationController::class, 'randomPerson']);

    });

    
});
