<?php

namespace Database\Seeders;

use App\Models\Abonne;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AbonneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Abonne::factory()->count(10)->create();
    }
}
