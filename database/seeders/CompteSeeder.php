<?php

namespace Database\Seeders;

use App\Models\Compte;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CompteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Compte::factory()
            ->count(30)
            ->create([
                'abonne_id' => function () {
                    return rand(1, 100) <= 40 ? \App\Models\Abonne::inRandomOrder()->first()->id : null;
                },
            ]);
    }
}
