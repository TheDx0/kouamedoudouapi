<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Compte>
 */
class CompteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            // "abonne_id" => fake()->randomElement($idabonnes),
            'libelle' => $this->faker->word,
            'description' => $this->faker->sentence,
            'agence' => $this->faker->numerify('#####'),
            'banque' => 'CI' . $this->faker->randomNumber(3, true),
            'numero' => $this->faker->numerify('###########'),
            'rib' => $this->faker->numerify('##'),
            'montant' => $this->faker->randomFloat(0, 10000, 10000000),
            'domiciliation' => $this->faker->streetAddress
        ];
    }
}
