<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comptes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('abonne_id')->nullable()->constrained('abonnes');
            $table->string('libelle');
            $table->string('description')->nullable();
            $table->string('agence')->length(5);
            $table->string('banque')->length(5);
            $table->string('numero')->length(11);
            $table->string('rib')->length(2);
            $table->double('montant');
            $table->string('domiciliation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comptes');
    }
};
