<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('personnes', function (Blueprint $table) {
            $table->id();
            $table->string('langue')->nullable();
            $table->enum('genre', ['Masculin', 'Féminin'])->nullable();
            $table->string('religion')->nullable();
            $table->string('pays')->nullable();
            $table->string('indicatif')->nullable();
            $table->string('region')->nullable();
            $table->boolean('internet')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('personnes');
    }
};
